package ${PACKAGE_NAME};

#parse("File Header.java")
public enum ${NAME} {
    //TODO enter roles here with a name and description
	${END}

	private String name;
	private String description;

	${NAME}(String name, String description) {
		this.name = name;
		this.description = description;
	}

	/**
	 * Get Role Name
	 *
	 * @return
	 */
	public String getRoleName() {
		return this.name;
	}

	/**
	 * Get Granted Authority which is role name
	 *
	 * @return
	 */
	public String getGrantedAuthority() {
		return this.name;
	}

	/**
	 * Get Role/Granted Authority description
	 *
	 * @return
	 */
	public String getRoleDescription() {
		return this.description;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		return this.name();
	}

}
