package ${PACKAGE_NAME};

import com.saucelabs.common.SauceOnDemandAuthentication;
import com.saucelabs.common.SauceOnDemandSessionIdProvider;
import com.saucelabs.junit.ConcurrentParameterized;
import com.saucelabs.junit.SauceOnDemandTestWatcher;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.*;
import org.junit.rules.TestName;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedList;

#parse("File Header.java")
@RunWith(ConcurrentParameterized.class)
public class ${NAME} implements SauceOnDemandSessionIdProvider {

	private static final Logger LOGGER = Logger.getLogger(${NAME}.class);

	public static final String SAUCE_USERNAME = System.getenv("SAUCE_USERNAME");
	public static final String SAUCE_ACCESS_KEY = System.getenv("SAUCE_ACCESS_KEY");
	public static final String SAUCE_ONDEMAND_BROWSERS = System.getenv("SAUCE_ONDEMAND_BROWSERS");
	public static final String APPLICATION_URL = "$appURL";


	/**
	 * Constructs a {@link SauceOnDemandAuthentication} instance using the supplied user name/access key.  To use the authentication
	 * supplied by environment variables or from an external file, use the no-arg {@link SauceOnDemandAuthentication} constructor.
	 */
	public SauceOnDemandAuthentication authentication = new SauceOnDemandAuthentication(SAUCE_USERNAME, SAUCE_ACCESS_KEY);

	/**
	 * JUnit Rule which will mark the Sauce Job as passed/failed when the test succeeds or fails.
	 */
	@Rule
	public SauceOnDemandTestWatcher resultReportingTestWatcher = new SauceOnDemandTestWatcher(this, authentication);

	@Rule
	public TestName name = new TestName() {
		public String getMethodName() {
			return String.format("%s : (%s %s %s)", super.getMethodName(), os, browser, version);
		}

		;
	};

	/**
	 * wait time waiting for something
	 */
	protected WebDriverWait wait;

	/**
	 * Represents the browser to be used as part of the test run.
	 */
	private String browser;
	/**
	 * Represents the operating system to be used as part of the test run.
	 */
	private String os;
	/**
	 * Represents the version of the browser to be used as part of the test run.
	 */
	private String version;
	/**
	 * Represents the deviceName of mobile device
	 */
	private String deviceName;
	/**
	 * Represents the device-orientation of mobile device
	 */
	private String deviceOrientation;
	/**
	 * Instance variable which contains the Sauce Job Id.
	 */
	private String sessionId;

	/**
	 * The {@link WebDriver} instance which is used to perform browser interactions with.
	 */
	private WebDriver driver;


	public ${NAME}(String os, String version, String browser, String deviceName, String deviceOrientation) {
		super();
		this.os = os;
		this.version = version;
		this.browser = browser;
		this.deviceName = deviceName;
		this.deviceOrientation = deviceOrientation;
	}

	/**
	 * @return a LinkedList containing String arrays representing the browser combinations the test should be run against. The values
	 * in the String array are used as part of the invocation of the test constructor
	 */
	@ConcurrentParameterized.Parameters
	public static LinkedList browsersStrings() {
		LinkedList browsers = new LinkedList();

		try {
			JSONArray browserData = new JSONArray(SAUCE_ONDEMAND_BROWSERS);
			for (int i = 0; i < browserData.length(); i++) {
				JSONObject browserObject = browserData.getJSONObject(i);
				browsers.add(new String[]{browserObject.getString("os"), browserObject.getString("browser-version"), browserObject.getString("browser"), null, null});
			}
		} catch (JSONException ex) {
			LOGGER.error(ex);
		}

		return browsers;
	}

	@Before
	public void setUp() throws MalformedURLException {
		DesiredCapabilities desiredCapabilities = new DesiredCapabilities();

		desiredCapabilities.setBrowserName(browser);
		desiredCapabilities.setVersion(version);
		desiredCapabilities.setCapability(CapabilityType.PLATFORM, os);
		desiredCapabilities.setCapability("name", name.getMethodName());
		desiredCapabilities.setCapability("build", System.getenv("JOB_NAME") + "_" + System.getenv("BUILD_NUMBER"));
		driver = new RemoteWebDriver(new URL("http://" + System.getenv("SAUCE_USERNAME") + ":" + System.getenv("SAUCE_ACCESS_KEY") + "@ondemand.saucelabs.com:80/wd/hub"), desiredCapabilities);

		// Setup wait
		wait = new WebDriverWait(driver, 30);

		LOGGER.debug("Going to test site");
		driver.get(APPLICATION_URL);

		login();
	}

	@After
	public void tearDown() {
		sessionId = (((RemoteWebDriver) driver).getSessionId()).toString();

		// Log out Session info
		String message = String.format("SauceOnDemandSessionID=$testName job-name=$testName", sessionId, System.getenv("JOB_NAME"));
		System.out.println(message);
		driver.quit();
	}

	/**
	 * Login to SSO
	 */
	private void login() {
		LOGGER.debug("Logging in");
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("INT_USERNAME")));

		WebElement usernameField = driver.findElement(By.id("INT_USERNAME"));
		WebElement passwordField = driver.findElement(By.id("INT_PASSWORD"));
		WebElement loginBtn = driver.findElement(By.id("INT_LOGIN_BTN"));

		LOGGER.debug("setting username");
		usernameField.clear();
		usernameField.sendKeys("entteamtest");

		LOGGER.debug("setting password");
		passwordField.clear();
		passwordField.sendKeys("Naicent1");

		LOGGER.debug("clicking login btn");
		loginBtn.click();

		// wait until CECI Admin screen is up
		wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText(AccountMaintenanceSearch.ADD_A_NEW_MASTER_ACCOUNT)));
	}
	
	// TODO add test
	

	@Override
	public String getSessionId() {
		return sessionId;
	}
}
