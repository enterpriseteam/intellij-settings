package ${PACKAGE_NAME};

import org.apache.log4j.Logger;
import org.naic.enterprise.security.filter.NAICSpringPreAuthFilter;
import org.naic.enterprise.security.filter.NAICSpringSecurityCsrfCookieFilter;
import org.naic.enterprise.security.handler.NAICSpringSecurityAccessDeniedHandler;
import org.naic.enterprise.security.service.NAICUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationProvider;
import org.springframework.security.web.authentication.preauth.RequestHeaderAuthenticationFilter;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.security.web.csrf.CsrfTokenRepository;

#parse("File Header.java")
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class ${NAME} extends WebSecurityConfigurerAdapter {

	@Autowired
	NAICUserDetailsService naicUserDetailsService;

	@Autowired
	SpringAuthenticationPoint authPoint;

	private static final Logger LOGGER = Logger.getLogger(${NAME}.class);

	@Bean
	public CsrfTokenRepository naicCsrfTokenRepository()
	{
		CookieCsrfTokenRepository repository = new CookieCsrfTokenRepository();
		repository.setHeaderName("X-XSRF-TOKEN");
		repository.setParameterName("_csrf");
		repository.setCookieName("XSRF-TOKEN");
		repository.setCookieHttpOnly(false);

		return repository;
	}

	public NAICSpringSecurityCsrfCookieFilter naicCookieCsrfFilter()
	{
		return new NAICSpringSecurityCsrfCookieFilter();
	}

	@Bean
	public NAICSpringPreAuthFilter naicAuthFilter()
	{
		LOGGER.debug("Creating Auth Filter");
		NAICSpringPreAuthFilter naicSpringSecurityAuthFilter = new NAICSpringPreAuthFilter();
		try
		{
			naicSpringSecurityAuthFilter.setAuthenticationManager(authenticationManagerBean());
		}
		catch (Exception e)
		{
			LOGGER.warn("Problem creating filter", e);
		}
		return naicSpringSecurityAuthFilter;
	}

	@Bean
	public AccessDeniedHandler naicAccessDeniedHandler()
	{
		return new NAICSpringSecurityAccessDeniedHandler();
	}

	/**
	 * Global Security Configuration. Set
	 * {@link PreAuthenticatedAuthenticationProvider} and
	 * {@link NAICUserDetailsService}
	 *
	 * @param auth
	 * @throws Exception
	 */
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception
	{
		auth.parentAuthenticationManager(authenticationManagerBean());

		LOGGER.debug("Creating provider");
		PreAuthenticatedAuthenticationProvider provider = new PreAuthenticatedAuthenticationProvider();

		LOGGER.debug("Assigning UserDetailsService to provider");
		provider.setPreAuthenticatedUserDetailsService(naicUserDetailsService);

		LOGGER.debug("Assinging provider to AuthenticationManagerBuilder");
		auth.authenticationProvider(provider);

		super.configure(auth);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.springframework.security.config.annotation.web.configuration.
	 * WebSecurityConfigurerAdapter#configure(org.springframework.security.
	 * config.annotation.web.builders.HttpSecurity)
	 */
	@Override
	protected void configure(HttpSecurity http) throws Exception
	{
		LOGGER.debug("Spring Security Config");
		http.addFilterAt(naicAuthFilter(), RequestHeaderAuthenticationFilter.class).authorizeRequests().antMatchers("/403", "/500", "/404", "/jquery/**", "/script/**", "/style/**", "/theme/**").permitAll().antMatchers("/")
				.hasAuthority(AppRoles.PAYMENT_GATEWAY_ADMIN_VIEW_PR.getGrantedAuthority()).and().csrf().csrfTokenRepository(naicCsrfTokenRepository()).and().exceptionHandling().authenticationEntryPoint(authPoint)
				.accessDeniedHandler(naicAccessDeniedHandler());
	}


}
